#!/usr/bin/env python
# -*- coding: utf-8 -*-

#%%
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as m3d
import vtk
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk

from local_to_utm import local2utm

#%%
filename = 'ert4_DefaultInversion'

# ert1
# utm_start = np.array([431509.1555,5473800.721])
# utm_end = np.array([431686.0806,5473956.287])

# # ert2
# utm_start = np.array([431078,5474232])
# utm_end = np.array([431376,5474372])

# # ert3
# utm_start = np.array([431485,5473901.9])
# utm_end = np.array([431576,5473979.3])

# # ert4
utm_start = np.array([431498.3,5473880.8])
utm_end = np.array([431591.3,5473957.3])

#%% Read vtk
reader = vtk.vtkUnstructuredGridReader()
reader.SetFileName(filename + '.vtk')
reader.ReadAllScalarsOn() # Read all scalars from VTK file
reader.Update()
grid = reader.GetOutput()

#%%
# # Create the transformation
# t = vtk.vtkTransform()
# t.Translate(-10, -10, 0.0)

# # Apply the transformation to the grid
# tf = vtk.vtkTransformFilter()
# tf.SetInputData(grid)    # Grid is of type vtkUnstructuredGrid()
# tf.SetTransform(t)
# tf.Update()
# grid.Modified()

# tf.GetOutput().GetBounds()

#%% Read points.
vtk_points = grid.GetPoints()
xyz3d = vtk_to_numpy( vtk_points.GetData() )

#%% Read cells.
# vtk_cells = grid.GetCells()
# vtk_cell_locations = grid.GetCellLocationsArray()
# vtk_cell_types = grid.GetCellTypesArray()
# vtk_cell_scalars = grid.GetCells().GetData()
# vtk_cell_data = grid.GetCellData()

#%% Convert cells in np arrays
# cells = vtk_to_numpy(vtk_cells)
# cell_locations = vtk_to_numpy(vtk_cell_locations)
# cell_types = vtk_to_numpy(vtk_cell_types)
# cell_scalars = vtk_to_numpy(vtk_cell_scalars)
# cell_data = vtk_to_numpy(vtk_cell_data)

#%% Convert coordinates
xnew, ynew = local2utm(xyz3d[:,0],xyz3d[:,2],utm_start,utm_end)

#%% Shift points.
xyz3d_shift = np.copy(xyz3d)
xyz3d_shift[:,0] = xnew
xyz3d_shift[:,1] = ynew
xyz3d_shift[:,2] = xyz3d[:,1]

#%%
# grid_shift = vtk.vtkUnstructuredGrid()
grid_shift = grid

#%% Set new points
vtk_points_shift = vtk.vtkPoints()
vtk_points_shift.SetData( numpy_to_vtk(xyz3d_shift) )
grid_shift.SetPoints(vtk_points_shift)

#%% Set Cells
# grid_shift.SetCells(vtk_cell_types, vtk_cell_locations, vtk_cells) 
# vtk_cell_shift_scalars = vtk_cell_scalars
# vtk_cell_shift_data = vtk_cell_data
# vtk_cell_shift_data.SetScalars(vtk_cell_shift_scalars)

# %%
writer = vtk.vtkUnstructuredGridWriter()
writer.SetFileName(filename + '_shift.vtk')
writer.SetInputData(grid_shift)
writer.Write()
