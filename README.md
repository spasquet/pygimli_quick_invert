# pygimli_quick_invert

Simple inversion of geophysical data with pygimli/pybert

## Installation 

See `environment.yml` for required packages, or follow instructions here : https://gitlab.com/resistivity-net/bert#installation-with-conda

## Scripts

### pybert_quick_invert.py

Reads datafile in `/data` and topofile in `/topo`, then performs pybert inversion with default settings.
Saves apparent electrical resistivity and inverted true electrical resistivity images and inverted true electrical resistivity in VTK format.

### shift_vtk.py

Reads inverted true electrical resistivity VTK file and shift X,Y coordinates from local to UTM system
