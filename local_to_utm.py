#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

def local2utm(xloc,yloc,utm_start,utm_end):
    
    # Create rotation matrix
    if utm_end[0]>utm_start[0]:
        theta=np.arctan((utm_end[1]-utm_start[1])/(utm_end[0]-utm_start[0]))
    else:
        theta=np.arctan((utm_end[1]-utm_start[1])/(utm_end[0]-utm_start[0])) + np.pi

    R = [[np.cos(theta),-np.sin(theta)], [np.sin(theta),np.cos(theta)]]
    
    # Rotate your point(s)
    rotpoint = np.matmul(R,(xloc,yloc))
    rotpoint = np.transpose(rotpoint)

    xnew = rotpoint[:,0]+utm_start[0]
    ynew = rotpoint[:,1]+utm_start[1]

    return xnew,ynew